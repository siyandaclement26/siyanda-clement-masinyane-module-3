import 'user.dart';

class UserPreferences {
  static const myUser = User(
    imagePath: 'https://lh3.googleusercontent.com/ogw/ADea4I6tdK91BMZJ6AbL9YRtoBfuA639S8lAJi1Y0iY6rg=s32-c-mo',
    name: 'Siyanda Clement Masinyane',
    email: 'siyandaclement26@gmail.com',
    about: ' I am one of the MTN Business Academy App 2022 Programme.',
    isDarkMode: false,
  );
}
